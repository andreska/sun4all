package com.andres.sun4all;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Locale;

import android.os.AsyncTask;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Main extends FragmentActivity {
	
	static boolean inicio = true;
	Bitmap bitmapBase, bitmapBaseNeg;

	String post = "http://sun4allmobile.socientize.eu/api/images";
	
	Context mainContext ;
	
	static String urlBase = "https://pybossa.socientize.eu/sun4all/sunimages/";
	static String urlBaseNeg="https://pybossa.socientize.eu/sun4all/sunimages/inv/";
	
	static public int contador = 0;
	static TextView txtCont;
	TextView txtSpots;
	//variables
	Imagen imagen;
	Button btnInv, btnFin, btnRes;
	ToggleButton btnAdd, btnRmv;

	LinearLayout layout1, layout2;
	String cadena;
	static String cadUrl;
	Editable strMove;
	Editable strAdd;

	static int width;
	static int height;
	static Drawable d;
	static int viewWidth;
	static int viewHeight;

	static boolean finish;
	Bitmap base;
	Bitmap nega;
	ProgressDialog mProgressDialog;
	URL urlNormal;
	URL urlNega;
	
	/** Return an array of 2 random Strings(url) (2 images), normal and inverted*/
	String[] getRandomUrl(){
		
		int aleatorio = (int) Math.floor(Math.random()*3494-1);
		cadUrl = getCadena(aleatorio);
		String[] cadenas = new String[2];
		String uno = urlBase.concat(cadUrl);
		String dos = urlBaseNeg.concat(cadUrl);
		cadenas[0]= uno;
		cadenas[1]=dos;
		return cadenas;
	}
	/** Return the String(url) in the line X from res/raw/imagenes_sol */
	String getCadena(int x){
		int resId = getResources().getIdentifier("imagenes_sol","raw", getPackageName());
		InputStream ins = getResources().openRawResource(resId);
		BufferedReader buf = new BufferedReader(new InputStreamReader(ins));
		
		for(int j=0;j<x+1;j++){
			try {
				buf.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		String cad = null;
		try {
			cad = buf.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(ins!=null){
			try {
				ins.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(buf!=null){
			try {
				buf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return cad;
	}
	/** onCreate*/
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		inicio=true;
		imagen = (Imagen) findViewById(R.id.ImgFoto);
		
		txtCont = (TextView) findViewById(R.id.txtCont);
		txtCont.setText(String.valueOf(contador));
		txtSpots = (TextView) findViewById(R.id.txtSpots);
		
		btnAdd =(ToggleButton)findViewById(R.id.btnAdd);
		btnRmv =(ToggleButton)findViewById(R.id.btnRmv);
		btnFin =(Button)findViewById(R.id.btnFin);
		btnInv =(Button)findViewById(R.id.btnInv);
		btnRes =(Button)findViewById(R.id.btnRes);

		btnAdd.setOnClickListener(clickToggle);
		btnRmv.setOnClickListener(clickToggle);
		btnFin.setOnClickListener(clickBoton);
		btnInv.setOnClickListener(clickBoton);
		btnRes.setOnClickListener(clickBoton);

		layout1 = (LinearLayout)findViewById(R.id.layout1);
		layout2 = (LinearLayout)findViewById(R.id.layour2);
		
		mainContext = getApplicationContext();
		
		
		/** Change the button add/move depending the language*/
		setLanguageAdd();
		activaBotonesInicio(false);
		changeColors();
	}
	/** Change to white if true/ black if false*/
	void changeColors(){
		Log.i("changeColors","imagen.inverted = "+imagen.inverted);
		if(!imagen.inverted){
			layout1.setBackgroundColor(Color.WHITE);
			layout2.setBackgroundColor(Color.WHITE);
			btnAdd.setTextColor(Color.BLACK);
			btnRmv.setTextColor(Color.BLACK);
			btnInv.setTextColor(Color.BLACK);
			btnFin.setTextColor(Color.BLACK);
			btnRes.setTextColor(Color.BLACK);
			txtCont.setTextColor(Color.BLACK);
			txtSpots.setTextColor(Color.BLACK);
			imagen.postInvalidate();
		}
		else
		{
			layout1.setBackgroundColor(Color.BLACK);
			layout2.setBackgroundColor(Color.BLACK);
			
			btnAdd.setTextColor(Color.WHITE);
			btnRmv.setTextColor(Color.WHITE);
			btnInv.setTextColor(Color.WHITE);
			btnFin.setTextColor(Color.WHITE);
			btnRes.setTextColor(Color.WHITE);
			txtCont.setTextColor(Color.WHITE);
			txtSpots.setTextColor(Color.WHITE);
			imagen.postInvalidate();
		}
		if(!btnAdd.isEnabled()){
			btnAdd.setTextColor(Color.GRAY);
		}
		if(!btnRmv.isEnabled()){
			btnRmv.setTextColor(Color.GRAY);
		}
		if(!btnInv.isEnabled()){
			btnInv.setTextColor(Color.GRAY);
		}
		if(!btnFin.isEnabled()){
			btnFin.setTextColor(Color.GRAY);
		}
		if(!btnRes.isEnabled()){
			btnRes.setTextColor(Color.GRAY);
		}
	}

	/**Clicks on toggleButtons(add, Rmv*/
	View.OnClickListener clickToggle = (new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch(v.getId()){
			case R.id.btnAdd:
				imagen.borra=false;
				if(btnAdd.isChecked())//add sunspot
				{
					btnAdd.setText(strAdd);
					imagen.pinta = true;
					Log.d("btnAdd.isChecked()",""+imagen.pinta);
				}
				else//move
				{
					btnAdd.setText(strMove);
					imagen.pinta = false;
					Log.d("btnAdd.isChecked()",""+imagen.pinta);
				}
				break;//fin case btnAdd

			case R.id.btnRmv:
				if(btnRmv.isChecked())
				{
					btnRmv.setText("OK");
					activaBotonesBorrar(false);
					imagen.borra=true;
					imagen.pinta = true;
				}
				else
				{
					btnRmv.setText(getResources().getString(R.string.btnRmv));
					activaBotonesBorrar(true);
					imagen.borra=false;
					if(btnAdd.isChecked())
						imagen.pinta=true;
					else
						imagen.pinta=false;
				}
				break;
			}
			changeColors();
		}
	});
	/** Clicks on Buttons(Fin, Inv, Res)*/
	View.OnClickListener clickBoton = (new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			switch(v.getId()){
			case R.id.btnFin:
				if(inicio){
					String [] cadenas = getRandomUrl();
					imagen.preparaDescarga(cadenas);
					imagen.postInvalidate();
					btnFin.setText(R.string.btnFin);
					activaBotonesInicio(true);
					inicio=false;
				}else{
					Dialogo dialogo = new Dialogo();
					dialogo.show(getSupportFragmentManager(), "tagAlerta");
				}
				break;
			/** invert image*/
			case R.id.btnInv:
				imagen.invertBitmap();
				break;
			case R.id.btnRes://reinicia imagen sin sunspots(start over)
				vaciaCoordenadas();
				imagen.setZoom(imagen.MIN_ZOOM);
			}
			changeColors();
			imagen.postInvalidate();
		}
	});
	/** clear imagen.listaPtos & imagen.listaMarcas; change btnAdd*/
	void vaciaCoordenadas(){
		imagen.listaPtos.clear();
		imagen.listaMarcas.clear();
		cambiaAdd(false);
	}
	/** change the string of add Button*/
	void cambiaAdd(boolean b){
		btnAdd.setChecked(b);
		imagen.pinta=b;
		if(b)
			btnAdd.setText(strAdd);
		else
			btnAdd.setText(strMove);
		Log.d("btnAdd.isChecked()",""+imagen.pinta);
	}
	/** Enable or disable buttons depending delete Button*/
	void activaBotonesBorrar(boolean b){
		btnAdd.setEnabled(b);
		btnInv.setEnabled(b);
		btnFin.setEnabled(b);
		btnRes.setEnabled(b);
	}
	/** Enable or disable buttons depending if the task is started*/
	void activaBotonesInicio(boolean b){
		btnAdd.setEnabled(b);
		btnRmv.setEnabled(b);
		btnInv.setEnabled(b);
		btnRes.setEnabled(b);
	}
	/** Show a toast with the message passed on 's' and 'ms' milliseconds*/
	void toast(String s, int ms){
		Toast.makeText(getApplicationContext(), s, ms).show();
	}
	/** Set the String and font of btnAdd depending user language*/
	void setLanguageAdd(){
		Locale current = getResources().getConfiguration().locale;
		/** español*/
		if (current.getLanguage().equals("es")){
			cadena = "Mover imagen/añadir mancha";
			strMove = Editable.Factory.getInstance().newEditable(cadena);
			strAdd = Editable.Factory.getInstance().newEditable(cadena);
			strMove.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,12,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			strAdd.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),13,26,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		/** italiano*/
		else if(current.getLanguage().equals("it")){
			cadena = "Spostare l'immagine/Aggiungi macchia solare";
			strMove = Editable.Factory.getInstance().newEditable(cadena);
			strAdd = Editable.Factory.getInstance().newEditable(cadena);
			strMove.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,19,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			strAdd.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),20,43,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		/** français*/
		else if(current.getLanguage().equals("fr")){
			cadena = "Déplacez l'image/Ajouter taches solaires";
			strMove = Editable.Factory.getInstance().newEditable(cadena);
			strAdd = Editable.Factory.getInstance().newEditable(cadena);
			strMove.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,16,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			strAdd.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),17,39,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		/** english(default)*/
		else{
			cadena = "Move image/Add Sunspot";
			strMove = Editable.Factory.getInstance().newEditable(cadena);
			strAdd = Editable.Factory.getInstance().newEditable(cadena);
			strMove.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0,10,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			strAdd.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),11,22,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		btnAdd.setText(strMove);
	}
	/** This class ask the user if he want finalize the task and start another*/
	class Dialogo extends DialogFragment {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState){
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(getResources().getString(R.string.dialogMessage))
			.setTitle(getResources().getString(R.string.dialogTitle))
			.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					//envia listaMarcas()+id Imagen
					imagen.prepareJson();
					JSONObject json = imagen.finalJson;
					try{
						//HttpResponse htRes = makeRequest(post, json);
						new makeRequest().execute(json);
						
					}catch(Exception e){
						e.printStackTrace();
					}
					//borra ptos y pasa a modo mover
					vaciaCoordenadas();
					btnFin.setText(R.string.btnFin2);
					activaBotonesInicio(false);
					inicio=true;
					dialog.cancel();
					imagen.inverted=false;
					vaciaCoordenadas();
					imagen.bitmap=imagen.inicial;
					changeColors();
					imagen.postInvalidate();
				}
			})
			.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();	
				}
			});
			return builder.create();
		}
	}
	
	ProgressDialog pDialog;
	
	class makeRequest extends AsyncTask<JSONObject, Void, Void> {
		/** doInBackground*/
		@Override
		protected Void doInBackground(JSONObject... params) {
			DefaultHttpClient httpclient = new DefaultHttpClient();
		    HttpPost httpost = new HttpPost(post);
		    JSONObject json = params[0];
		    StringEntity se = null;
		    try{
		    se = new StringEntity(json.toString());
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
		    httpost.setEntity(se);
		    httpost.setHeader("Accept", "application/json");
		    httpost.setHeader("Content-type", "application/json");
		    //Handles what is returned from the page 
		    ResponseHandler responseHandler = new BasicResponseHandler();
		    
		    try {
				httpclient.execute(httpost, responseHandler);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		    
			return null;
		}
	}
	public static String getResponseBody(HttpResponse response) {
		String response_text = null;
		HttpEntity entity = null;
		try {
			entity = response.getEntity();
			response_text = _getResponseBody(entity);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			if (entity != null) {
				try {
					entity.consumeContent();
				} catch (IOException e1) {}
			}
		}
		return response_text;
	}

	public static String _getResponseBody(final HttpEntity entity) throws IOException, ParseException {
		if (entity == null) { throw new IllegalArgumentException("HTTP entity may not be null"); }
		InputStream instream = entity.getContent();
		if (instream == null) { return ""; }
		if (entity.getContentLength() > Integer.MAX_VALUE) { throw new IllegalArgumentException(
				"HTTP entity too large to be buffered in memory"); }
		String charset = getContentCharSet(entity);
		if (charset == null) {
			charset = HTTP.DEFAULT_CONTENT_CHARSET;
		}
		Reader reader = new InputStreamReader(instream, charset);
		StringBuilder buffer = new StringBuilder();
		try {
			char[] tmp = new char[1024];
			int l;
			while ((l = reader.read(tmp)) != -1) {
				buffer.append(tmp, 0, l);
			}
		} finally {
			reader.close();
		}
		return buffer.toString();
	}
	public static String getContentCharSet(final HttpEntity entity) throws ParseException {
		if (entity == null) { throw new IllegalArgumentException("HTTP entity may not be null"); }
		String charset = null;
		if (entity.getContentType() != null) {
			HeaderElement values[] = entity.getContentType().getElements();
			if (values.length > 0) {
				NameValuePair param = values[0].getParameterByName("charset");
				if (param != null) {
					charset = param.getValue();
				}
			}
		}
		return charset;
	}
}